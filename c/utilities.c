#define PY_SSIZE_T_CLEAN 
#include <path/to/Python.h>
#include <path/to/math.h>
#include <path/to/arrayobject.h>
#include <stdlib.h>
#include <stdio.h>
#define SIZE 3

double Cutoff(double input) {
    if (-input < -50) {
        return 0.0;
    } else {
        return exp(-input);
    }
};
double Cutoff_erf(double input_erf) {
    if (input_erf > 5) {
        return 1.0;
    } else {
        return erf(input_erf);
    }
};


void expand_radius(const long* Nc_v, double r_m[][3], 
               double r_o[3], const double Rc_v[3],
               int noc){
  /* Make nearest neighbor matrix of distances
  */
  for (int l = 0; l < noc; l++){
    const long* Nc = Nc_v + l * 3;
    r_m[l][0] = r_o[0] + Nc[0] * Rc_v[0];
    r_m[l][1] = r_o[1] + Nc[1] * Rc_v[1];
    r_m[l][2] = r_o[2] + Nc[2] * Rc_v[2];
  }
}

void generate_full_matrix(int d, double uniqueEntries[], double result_vec[]) {
  if (d == 10) {
    int entryIndex = 0;
    for (int i = 0; i < 3; i++) {
      for (int j = i; j < 3; j++) {
        for (int k = j; k < 3; k++) {
          int index = i * 3 * 3 + j * 3 + k;
          result_vec[index] = uniqueEntries[entryIndex];
          result_vec[i * 3 * 3 + k * 3 + j] = uniqueEntries[entryIndex];
          result_vec[j * 3 * 3 + i * 3 + k] = uniqueEntries[entryIndex];
          result_vec[j * 3 * 3 + k * 3 + i] = uniqueEntries[entryIndex];
          result_vec[k * 3 * 3 + i * 3 + j] = uniqueEntries[entryIndex];
          result_vec[k * 3 * 3 + j * 3 + i] = uniqueEntries[entryIndex];
          entryIndex++;
        }
      }
    }
  } else if (d == 15) {
    int entryIndex = 0;
    for (int i = 0; i < 3; i++) {
        for (int j = i; j < 3; j++) {
            for (int k = j; k < 3; k++) {
                for (int l = k; l < 3; l++) {
                    result_vec[i * SIZE * SIZE * SIZE + j * SIZE * SIZE + k * SIZE + l] = uniqueEntries[entryIndex];
                    result_vec[i * SIZE * SIZE * SIZE + l * SIZE * SIZE + k * SIZE + j] = uniqueEntries[entryIndex];
                    result_vec[i * SIZE * SIZE * SIZE + k * SIZE * SIZE + l * SIZE + j] = uniqueEntries[entryIndex];
                    result_vec[i * SIZE * SIZE * SIZE + k * SIZE * SIZE + j * SIZE + l] = uniqueEntries[entryIndex];

                    result_vec[j * SIZE * SIZE * SIZE + i * SIZE * SIZE + l * SIZE + k] = uniqueEntries[entryIndex];
                    result_vec[j * SIZE * SIZE * SIZE + l * SIZE * SIZE + i * SIZE + k] = uniqueEntries[entryIndex];
                    result_vec[j * SIZE * SIZE * SIZE + k * SIZE * SIZE + l * SIZE + i] = uniqueEntries[entryIndex];

                    result_vec[k * SIZE * SIZE * SIZE + j * SIZE * SIZE + i * SIZE + l] = uniqueEntries[entryIndex];
                    result_vec[k * SIZE * SIZE * SIZE + i * SIZE * SIZE + j * SIZE + l] = uniqueEntries[entryIndex];
                    result_vec[k * SIZE * SIZE * SIZE + l * SIZE * SIZE + j * SIZE + i] = uniqueEntries[entryIndex];
                    result_vec[k * SIZE * SIZE * SIZE + j * SIZE * SIZE + l * SIZE + i] = uniqueEntries[entryIndex];

                    result_vec[l * SIZE * SIZE * SIZE + i * SIZE * SIZE + k * SIZE + j] = uniqueEntries[entryIndex];
                    result_vec[l * SIZE * SIZE * SIZE + j * SIZE * SIZE + k * SIZE + i] = uniqueEntries[entryIndex];
                    result_vec[l * SIZE * SIZE * SIZE + i * SIZE * SIZE + j * SIZE + k] = uniqueEntries[entryIndex];
                    result_vec[l * SIZE * SIZE * SIZE + k * SIZE * SIZE + i * SIZE + j] = uniqueEntries[entryIndex];   
                    entryIndex++;
                }
            }
        }
    }
  } else if (d == 21) {
    for (int p=0; p<243;p++) {
      result_vec[p] = 0;
    }
    int entryIndex = 0;
    for (int i = 0; i < 3; i++) {
        for (int j = i; j < 3; j++) {
            for (int k = j; k < 3; k++) {
                for (int l = k; l < 3; l++) {
                    for (int m = l; m < 3; m++) {
                        result_vec[i * 3 * 3 * 3 * 3 + j * 3 * 3 * 3 + k * 3 * 3 + l * 3 + m] = uniqueEntries[entryIndex];
                        result_vec[i * 3 * 3 * 3 * 3 + j * 3 * 3 * 3 + k * 3 * 3 + m * 3 + l] = uniqueEntries[entryIndex];
                        result_vec[i * 3 * 3 * 3 * 3 + j * 3 * 3 * 3 + l * 3 * 3 + k * 3 + m] = uniqueEntries[entryIndex];
                        result_vec[i * 3 * 3 * 3 * 3 + j * 3 * 3 * 3 + l * 3 * 3 + m * 3 + k] = uniqueEntries[entryIndex];
                        result_vec[i * 3 * 3 * 3 * 3 + j * 3 * 3 * 3 + m * 3 * 3 + k * 3 + l] = uniqueEntries[entryIndex];
                        result_vec[i * 3 * 3 * 3 * 3 + k * 3 * 3 * 3 + l * 3 * 3 + j * 3 + m] = uniqueEntries[entryIndex];
                        result_vec[i * 3 * 3 * 3 * 3 + j * 3 * 3 * 3 + m * 3 * 3 + l * 3 + k] = uniqueEntries[entryIndex];
                        result_vec[i * 3 * 3 * 3 * 3 + k * 3 * 3 * 3 + j * 3 * 3 + l * 3 + m] = uniqueEntries[entryIndex];
                        result_vec[i * 3 * 3 * 3 * 3 + k * 3 * 3 * 3 + l * 3 * 3 + m * 3 + j] = uniqueEntries[entryIndex];
                        result_vec[i * 3 * 3 * 3 * 3 + k * 3 * 3 * 3 + j * 3 * 3 + m * 3 + l] = uniqueEntries[entryIndex];
                        result_vec[i * 3 * 3 * 3 * 3 + l * 3 * 3 * 3 + k * 3 * 3 + j * 3 + m] = uniqueEntries[entryIndex];
                        result_vec[i * 3 * 3 * 3 * 3 + m * 3 * 3 * 3 + l * 3 * 3 + j * 3 + k] = uniqueEntries[entryIndex];
                        result_vec[i * 3 * 3 * 3 * 3 + l * 3 * 3 * 3 + k * 3 * 3 + m * 3 + j] = uniqueEntries[entryIndex];
                        result_vec[i * 3 * 3 * 3 * 3 + l * 3 * 3 * 3 + j * 3 * 3 + m * 3 + k] = uniqueEntries[entryIndex];
                        result_vec[i * 3 * 3 * 3 * 3 + l * 3 * 3 * 3 + m * 3 * 3 + k * 3 + j] = uniqueEntries[entryIndex];
                        result_vec[i * 3 * 3 * 3 * 3 + l * 3 * 3 * 3 + m * 3 * 3 + j * 3 + k] = uniqueEntries[entryIndex];
                        result_vec[i * 3 * 3 * 3 * 3 + m * 3 * 3 * 3 + j * 3 * 3 + k * 3 + l] = uniqueEntries[entryIndex];
                        result_vec[i * 3 * 3 * 3 * 3 + m * 3 * 3 * 3 + j * 3 * 3 + l * 3 + k] = uniqueEntries[entryIndex];
                        result_vec[i * 3 * 3 * 3 * 3 + m * 3 * 3 * 3 + l * 3 * 3 + k * 3 + j] = uniqueEntries[entryIndex];
                        result_vec[i * 3 * 3 * 3 * 3 + m * 3 * 3 * 3 + k * 3 * 3 + l * 3 + j] = uniqueEntries[entryIndex];
                        result_vec[i * 3 * 3 * 3 * 3 + m * 3 * 3 * 3 + k * 3 * 3 + j * 3 + l] = uniqueEntries[entryIndex];

                        result_vec[j * 3 * 3 * 3 * 3 + k * 3 * 3 * 3 + i * 3 * 3 + l * 3 + m] = uniqueEntries[entryIndex];
                        result_vec[j * 3 * 3 * 3 * 3 + k * 3 * 3 * 3 + l * 3 * 3 + i * 3 + m] = uniqueEntries[entryIndex];
                        result_vec[j * 3 * 3 * 3 * 3 + k * 3 * 3 * 3 + l * 3 * 3 + m * 3 + i] = uniqueEntries[entryIndex];
                        result_vec[j * 3 * 3 * 3 * 3 + k * 3 * 3 * 3 + m * 3 * 3 + l * 3 + i] = uniqueEntries[entryIndex];
                        result_vec[j * 3 * 3 * 3 * 3 + l * 3 * 3 * 3 + k * 3 * 3 + m * 3 + i] = uniqueEntries[entryIndex];
                        result_vec[j * 3 * 3 * 3 * 3 + l * 3 * 3 * 3 + i * 3 * 3 + m * 3 + k] = uniqueEntries[entryIndex];
                        result_vec[j * 3 * 3 * 3 * 3 + i * 3 * 3 * 3 + m * 3 * 3 + k * 3 + l] = uniqueEntries[entryIndex];
                        result_vec[j * 3 * 3 * 3 * 3 + i * 3 * 3 * 3 + m * 3 * 3 + l * 3 + k] = uniqueEntries[entryIndex];
                        result_vec[j * 3 * 3 * 3 * 3 + l * 3 * 3 * 3 + m * 3 * 3 + i * 3 + k] = uniqueEntries[entryIndex];
                        result_vec[j * 3 * 3 * 3 * 3 + l * 3 * 3 * 3 + i * 3 * 3 + k * 3 + m] = uniqueEntries[entryIndex];
                        result_vec[j * 3 * 3 * 3 * 3 + l * 3 * 3 * 3 + k * 3 * 3 + i * 3 + m] = uniqueEntries[entryIndex];
                        result_vec[j * 3 * 3 * 3 * 3 + i * 3 * 3 * 3 + k * 3 * 3 + l * 3 + m] = uniqueEntries[entryIndex];
                        result_vec[j * 3 * 3 * 3 * 3 + l * 3 * 3 * 3 + m * 3 * 3 + k * 3 + i] = uniqueEntries[entryIndex];
                        result_vec[j * 3 * 3 * 3 * 3 + i * 3 * 3 * 3 + l * 3 * 3 + m * 3 + k] = uniqueEntries[entryIndex];
                        result_vec[j * 3 * 3 * 3 * 3 + i * 3 * 3 * 3 + k * 3 * 3 + m * 3 + l] = uniqueEntries[entryIndex];
                        result_vec[j * 3 * 3 * 3 * 3 + i * 3 * 3 * 3 + l * 3 * 3 + k * 3 + m] = uniqueEntries[entryIndex];

                        result_vec[k * 3 * 3 * 3 * 3 + j * 3 * 3 * 3 + l * 3 * 3 + i * 3 + m] = uniqueEntries[entryIndex];
                        result_vec[k * 3 * 3 * 3 * 3 + j * 3 * 3 * 3 + l * 3 * 3 + m * 3 + i] = uniqueEntries[entryIndex];
                        result_vec[k * 3 * 3 * 3 * 3 + j * 3 * 3 * 3 + m * 3 * 3 + l * 3 + i] = uniqueEntries[entryIndex];
                        result_vec[k * 3 * 3 * 3 * 3 + j * 3 * 3 * 3 + m * 3 * 3 + i * 3 + l] = uniqueEntries[entryIndex];
                        result_vec[k * 3 * 3 * 3 * 3 + m * 3 * 3 * 3 + i * 3 * 3 + l * 3 + j] = uniqueEntries[entryIndex];
                        result_vec[k * 3 * 3 * 3 * 3 + m * 3 * 3 * 3 + j * 3 * 3 + i * 3 + l] = uniqueEntries[entryIndex];
                        result_vec[k * 3 * 3 * 3 * 3 + m * 3 * 3 * 3 + j * 3 * 3 + l * 3 + i] = uniqueEntries[entryIndex];
                        result_vec[k * 3 * 3 * 3 * 3 + j * 3 * 3 * 3 + i * 3 * 3 + m * 3 + l] = uniqueEntries[entryIndex];
                        result_vec[k * 3 * 3 * 3 * 3 + j * 3 * 3 * 3 + i * 3 * 3 + l * 3 + m] = uniqueEntries[entryIndex];
                        result_vec[k * 3 * 3 * 3 * 3 + m * 3 * 3 * 3 + i * 3 * 3 + j * 3 + l] = uniqueEntries[entryIndex];
                        result_vec[k * 3 * 3 * 3 * 3 + l * 3 * 3 * 3 + i * 3 * 3 + j * 3 + m] = uniqueEntries[entryIndex];
                        result_vec[k * 3 * 3 * 3 * 3 + l * 3 * 3 * 3 + i * 3 * 3 + m * 3 + j] = uniqueEntries[entryIndex];
                        result_vec[k * 3 * 3 * 3 * 3 + l * 3 * 3 * 3 + m * 3 * 3 + i * 3 + j] = uniqueEntries[entryIndex];
                        result_vec[k * 3 * 3 * 3 * 3 + l * 3 * 3 * 3 + m * 3 * 3 + j * 3 + i] = uniqueEntries[entryIndex];
                        result_vec[k * 3 * 3 * 3 * 3 + l * 3 * 3 * 3 + j * 3 * 3 + m * 3 + i] = uniqueEntries[entryIndex];
                        result_vec[k * 3 * 3 * 3 * 3 + l * 3 * 3 * 3 + j * 3 * 3 + i * 3 + m] = uniqueEntries[entryIndex];
                        result_vec[k * 3 * 3 * 3 * 3 + m * 3 * 3 * 3 + l * 3 * 3 + j * 3 + i] = uniqueEntries[entryIndex];
                        result_vec[k * 3 * 3 * 3 * 3 + i * 3 * 3 * 3 + j * 3 * 3 + l * 3 + m] = uniqueEntries[entryIndex];
                        result_vec[k * 3 * 3 * 3 * 3 + i * 3 * 3 * 3 + j * 3 * 3 + m * 3 + l] = uniqueEntries[entryIndex];
                        result_vec[k * 3 * 3 * 3 * 3 + m * 3 * 3 * 3 + l * 3 * 3 + i * 3 + j] = uniqueEntries[entryIndex];

                        result_vec[m * 3 * 3 * 3 * 3 + i * 3 * 3 * 3 + j * 3 * 3 + k * 3 + l] = uniqueEntries[entryIndex];
                        result_vec[m * 3 * 3 * 3 * 3 + j * 3 * 3 * 3 + l * 3 * 3 + i * 3 + k] = uniqueEntries[entryIndex];
                        result_vec[m * 3 * 3 * 3 * 3 + l * 3 * 3 * 3 + i * 3 * 3 + k * 3 + j] = uniqueEntries[entryIndex];
                        result_vec[m * 3 * 3 * 3 * 3 + i * 3 * 3 * 3 + k * 3 * 3 + j * 3 + l] = uniqueEntries[entryIndex];
                        result_vec[m * 3 * 3 * 3 * 3 + i * 3 * 3 * 3 + j * 3 * 3 + l * 3 + k] = uniqueEntries[entryIndex];
                        result_vec[m * 3 * 3 * 3 * 3 + l * 3 * 3 * 3 + k * 3 * 3 + j * 3 + i] = uniqueEntries[entryIndex];
                        result_vec[m * 3 * 3 * 3 * 3 + i * 3 * 3 * 3 + k * 3 * 3 + l * 3 + j] = uniqueEntries[entryIndex];
                        result_vec[m * 3 * 3 * 3 * 3 + l * 3 * 3 * 3 + i * 3 * 3 + j * 3 + k] = uniqueEntries[entryIndex];
                        result_vec[m * 3 * 3 * 3 * 3 + k * 3 * 3 * 3 + l * 3 * 3 + j * 3 + i] = uniqueEntries[entryIndex];
                        result_vec[m * 3 * 3 * 3 * 3 + l * 3 * 3 * 3 + k * 3 * 3 + i * 3 + j] = uniqueEntries[entryIndex];
                        result_vec[m * 3 * 3 * 3 * 3 + j * 3 * 3 * 3 + l * 3 * 3 + k * 3 + i] = uniqueEntries[entryIndex];
                        result_vec[m * 3 * 3 * 3 * 3 + j * 3 * 3 * 3 + k * 3 * 3 + i * 3 + l] = uniqueEntries[entryIndex];
                        result_vec[m * 3 * 3 * 3 * 3 + j * 3 * 3 * 3 + i * 3 * 3 + l * 3 + k] = uniqueEntries[entryIndex];
                        result_vec[m * 3 * 3 * 3 * 3 + j * 3 * 3 * 3 + i * 3 * 3 + k * 3 + l] = uniqueEntries[entryIndex];

                        result_vec[l * 3 * 3 * 3 * 3 + k * 3 * 3 * 3 + j * 3 * 3 + i * 3 + m] = uniqueEntries[entryIndex];
                        result_vec[l * 3 * 3 * 3 * 3 + k * 3 * 3 * 3 + j * 3 * 3 + m * 3 + i] = uniqueEntries[entryIndex];
                        result_vec[l * 3 * 3 * 3 * 3 + j * 3 * 3 * 3 + m * 3 * 3 + k * 3 + i] = uniqueEntries[entryIndex];
                        result_vec[l * 3 * 3 * 3 * 3 + j * 3 * 3 * 3 + i * 3 * 3 + m * 3 + k] = uniqueEntries[entryIndex];
                        result_vec[l * 3 * 3 * 3 * 3 + m * 3 * 3 * 3 + j * 3 * 3 + k * 3 + i] = uniqueEntries[entryIndex];
                        result_vec[l * 3 * 3 * 3 * 3 + i * 3 * 3 * 3 + m * 3 * 3 + k * 3 + j] = uniqueEntries[entryIndex];
                        result_vec[l * 3 * 3 * 3 * 3 + i * 3 * 3 * 3 + m * 3 * 3 + j * 3 + k] = uniqueEntries[entryIndex];
                        result_vec[l * 3 * 3 * 3 * 3 + i * 3 * 3 * 3 + j * 3 * 3 + k * 3 + m] = uniqueEntries[entryIndex];
                        result_vec[l * 3 * 3 * 3 * 3 + i * 3 * 3 * 3 + k * 3 * 3 + j * 3 + m] = uniqueEntries[entryIndex];
                        result_vec[l * 3 * 3 * 3 * 3 + i * 3 * 3 * 3 + j * 3 * 3 + m * 3 + k] = uniqueEntries[entryIndex];
                        result_vec[l * 3 * 3 * 3 * 3 + j * 3 * 3 * 3 + i * 3 * 3 + k * 3 + m] = uniqueEntries[entryIndex];
                        result_vec[l * 3 * 3 * 3 * 3 + j * 3 * 3 * 3 + k * 3 * 3 + m * 3 + i] = uniqueEntries[entryIndex];
                        result_vec[l * 3 * 3 * 3 * 3 + k * 3 * 3 * 3 + m * 3 * 3 + j * 3 + i] = uniqueEntries[entryIndex];
                        result_vec[l * 3 * 3 * 3 * 3 + k * 3 * 3 * 3 + m * 3 * 3 + i * 3 + j] = uniqueEntries[entryIndex];
                        result_vec[l * 3 * 3 * 3 * 3 + k * 3 * 3 * 3 + i * 3 * 3 + m * 3 + j] = uniqueEntries[entryIndex];
                        result_vec[l * 3 * 3 * 3 * 3 + k * 3 * 3 * 3 + i * 3 * 3 + j * 3 + m] = uniqueEntries[entryIndex];
                        result_vec[l * 3 * 3 * 3 * 3 + j * 3 * 3 * 3 + k * 3 * 3 + i * 3 + m] = uniqueEntries[entryIndex];
                        result_vec[l * 3 * 3 * 3 * 3 + j * 3 * 3 * 3 + m * 3 * 3 + i * 3 + k] = uniqueEntries[entryIndex];
                        entryIndex++;
                    }
                }
            }
        }
    }
  }
}

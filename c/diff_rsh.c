#define PY_SSIZE_T_CLEAN
#include <path/to/Python.h>
#include <path/to/math.h>
#include <stdlib.h>
#include <stdio.h>
#include <path/to/arrayobject.h>

PyObject* diff_3(PyObject *self, PyObject *args);
PyObject* diff_4(PyObject *self, PyObject *args);
PyObject* diff_5(PyObject *self, PyObject *args);
PyObject* diff_1_and_2(PyObject *self, PyObject *args);

static PyMethodDef functions[] = {
    {"diff_3", diff_3, METH_VARARGS, 0},
    {"diff_4", diff_4, METH_VARARGS, 0},
    {"diff_5", diff_5, METH_VARARGS, 0},
    {"diff_1_and_2", diff_1_and_2, METH_VARARGS, 0},
    {0,0,0,0}
};

static struct PyModuleDef moduledef = {
    PyModuleDef_HEAD_INIT,
    "diff_rsh",
    "C-extension for RSH",
    -1,
    functions,   
    NULL
};

static PyObject* moduleinit(void)
{
    PyObject* m = PyModule_Create(&moduledef);
    if (m == NULL)
        return NULL;

    return m;
}
// Initialize the module
PyMODINIT_FUNC PyInit_diff_rsh(void) {
    import_array();
    return PyModule_Create(&moduledef);
}


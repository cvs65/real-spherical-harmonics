#define PY_SSIZE_T_CLEAN 
#include <stdlib.h>
#include <stdio.h>

double Cutoff(double input);
double Cutoff_erf(double input_erf);
void expand_radius(const long* Nc_v, double r_m[][3], 
               double r_o[3], const double Rc_v[3],
               int noc);
void generate_full_matrix(int d, double uniqueEntries[], double result_vec[]);

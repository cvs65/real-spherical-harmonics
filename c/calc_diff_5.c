#define PY_SSIZE_T_CLEAN
#include <path/to/Python.h>
#include <path/to/math.h>
#include <path/to/arrayobject.h>
#include "utilities.h"
#include "calc_diff_5_d_q.h"
#include "calc_diff_5_o_h.h"
#include <stdlib.h>
#include <stdio.h>
PyObject* diff_5(PyObject* self, PyObject* args) { 
    double a; // width of gaussian 
    PyArrayObject* mm_obj;
    PyArrayObject* Nc_v_obj;
    PyArrayObject* Rc_v_obj;
    PyArrayObject* coeffs_obj;
    PyArrayObject* QM_cell_mid_obj;
    PyArrayObject* result_obj;

    if(!PyArg_ParseTuple(args, "dOOOOOO", &a, &mm_obj, &result_obj, &coeffs_obj, &QM_cell_mid_obj, &Nc_v_obj, &Rc_v_obj))
        return NULL;
    const double *R_pv = PyArray_DATA(mm_obj);
    double *result = PyArray_DATA(result_obj);
    const long *Nc_v = PyArray_DATA(Nc_v_obj);
    const double *Rc_v = PyArray_DATA(Rc_v_obj);
    const double *coeffs = PyArray_DATA(coeffs_obj);
    const double *QM_cell_mid = PyArray_DATA(QM_cell_mid_obj);
    int noc = PyArray_DIM(Nc_v_obj, 0);
    int np = PyArray_DIM(mm_obj, 0);
    double r_o[3];
    double r_m[noc][3];
    double* d_data = (double*) calloc(63, sizeof(double));
    double* q_data = (double*) calloc(105, sizeof(double));
    double* o_data = (double*) calloc(147, sizeof(double));
    double* h_data = (double*) calloc(189, sizeof(double));
    for (int p = 0; p < np; p++) {
        const double* R_v = R_pv + 3 * p;
        double* result_vec =  result + 21 * p;
        r_o[0] = R_v[0] - QM_cell_mid[0];
        r_o[1] = R_v[1] - QM_cell_mid[1];
        r_o[2] = R_v[2] - QM_cell_mid[2];
        expand_radius(Nc_v, r_m, r_o, Rc_v, noc);
        for (int w = 0; w < noc; w++) {
            double x = r_m[w][0];
            double y = r_m[w][1];
            double z = r_m[w][2];
            double a_05 = pow(a,0.5);
            double a_15 = a*a_05;
            double a_25 = a*a_15;
            double a_35 = a*a_25;
            double a_45 = a*a_35;
            double a_55 = a*a_45;
            double a_2 = a*a;
            double a_3 = a*a_2;
            double a_4 = a*a_3;
            double a_5 = a*a_4;
            double x_sq = x*x;
            double y_sq = y*y;
            double z_sq = z*z;
            double x_cub = x*x_sq;
            double y_cub = y*y_sq;
            double z_cub = z*z_sq;
            double x_quat = x*x_cub;
            double y_quat = y*y_cub;
            double z_quat = z*z_cub;
            double x_pent = x*x_quat;
            double y_pent = y*y_quat;
            double z_pent = z*z_quat;
            double x_hex = x*x_pent;
            double y_hex = y*y_pent;
            double z_hex = z*z_pent;
            double r_2 = (x_sq + y_sq + z_sq);
            double r = pow(r_2,0.5);
            double ar = a_05*r;
            double ar_sq = a*r_2;
            double ar_15 = ar*ar_sq;
            double ar_25 = ar_15*ar_sq;
            double ar_35 = ar_25*ar_sq;
            double ar_45 = ar_35*ar_sq;
            double r_15 = r_2*r;
            double r_25 = r_2*r_15;
            double r_35 = r_2*r_25;
            double r_45 = r_2*r_35;
            double r_55 = r_2*r_45;
            double r_65 = r_2*r_55;
            double r_75 = r_2*r_65;
            double r_85 = r_2*r_75;
            double r_4 = r_2*r_2;
            double r_6 = r_4*r_2;
            double r_8 = r_6*r_2;
            double r_10 = r_8*r_2;
            double r_95 = r_2*r_85;
            double r_12 = r_10*r_2;
            double r_14 = r_12*r_2;
            double x_y_2 = (x_sq + y_sq);
            double x_y_4 = x_y_2*x_y_2;
            double x_y_6 = x_y_4*x_y_2;
            double x_y_8 = x_y_6*x_y_2;
            double Gaus_neg = Cutoff(ar_sq);
            double Erf = Cutoff_erf(ar);

            calc_d5_d_q(d_data, q_data, x, y, z, a, a_05, a_15, a_25, a_35, a_45, a_55, a_2, a_3,
                        a_4,a_5,x_sq,y_sq,z_sq,x_cub,y_cub,z_cub,x_quat,y_quat,z_quat,x_pent,y_pent,z_pent,
                        x_hex,y_hex,z_hex,r_2,r,ar,ar_sq,ar_15,ar_25,ar_35,ar_45,r_15,r_25,r_35,r_45,r_55,
                        r_65,r_75,r_85,r_4,r_6,r_8,r_10,r_95,r_12,r_14,x_y_2,x_y_4,x_y_6,x_y_8,Gaus_neg,Erf);

            calc_d5_o_h(o_data, h_data, x, y, z, a, a_05, a_15, a_25, a_35, a_45, a_55, a_2, a_3,
                        a_4,a_5,x_sq,y_sq,z_sq,x_cub,y_cub,z_cub,x_quat,y_quat,z_quat,x_pent,y_pent,z_pent,
                        x_hex,y_hex,z_hex,r_2,r,ar,ar_sq,ar_15,ar_25,ar_35,ar_45,r_15,r_25,r_35,r_45,r_55,
                        r_65,r_75,r_85,r_4,r_6,r_8,r_10,r_95,r_12,r_14,x_y_2,x_y_4,x_y_6,x_y_8,Gaus_neg,Erf);
            }

        double* uniqueEntries = (double*) calloc(21, sizeof(double));
        uniqueEntries[0] += (d_data[0]*coeffs[1] + d_data[21]*coeffs[2] + d_data[42]*coeffs[3]); // m=-1 + m=0 + m=1 
        uniqueEntries[1] += (d_data[1]*coeffs[1] + d_data[22]*coeffs[2] + d_data[43]*coeffs[3]); // m=-1 + m=0 + m=1 
        uniqueEntries[2] += (d_data[2]*coeffs[1] + d_data[23]*coeffs[2] + d_data[44]*coeffs[3]); // m=-1 + m=0 + m=1 
        uniqueEntries[3] += (d_data[3]*coeffs[1] + d_data[24]*coeffs[2] + d_data[45]*coeffs[3]); // m=-1 + m=0 + m=1 
        uniqueEntries[4] += (d_data[4]*coeffs[1] + d_data[25]*coeffs[2] + d_data[46]*coeffs[3]); // m=-1 + m=0 + m=1 
        uniqueEntries[5] += (d_data[5]*coeffs[1] + d_data[26]*coeffs[2] + d_data[47]*coeffs[3]); // m=-1 + m=0 + m=1 
        uniqueEntries[6] += (d_data[6]*coeffs[1] + d_data[27]*coeffs[2] + d_data[48]*coeffs[3]); // m=-1 + m=0 + m=1 
        uniqueEntries[7] += (d_data[7]*coeffs[1] + d_data[28]*coeffs[2] + d_data[49]*coeffs[3]); // m=-1 + m=0 + m=1 
        uniqueEntries[8] += (d_data[8]*coeffs[1] + d_data[29]*coeffs[2] + d_data[50]*coeffs[3]); // m=-1 + m=0 + m=1 
        uniqueEntries[9] += (d_data[9]*coeffs[1] + d_data[30]*coeffs[2] + d_data[51]*coeffs[3]); // m=-1 + m=0 + m=1 
        uniqueEntries[10] += (d_data[10]*coeffs[1] + d_data[31]*coeffs[2] + d_data[52]*coeffs[3]); // m=-1 + m=0 + m=1 
        uniqueEntries[11] += (d_data[11]*coeffs[1] + d_data[32]*coeffs[2] + d_data[53]*coeffs[3]); // m=-1 + m=0 + m=1 
        uniqueEntries[12] += (d_data[12]*coeffs[1] + d_data[33]*coeffs[2] + d_data[54]*coeffs[3]); // m=-1 + m=0 + m=1 
        uniqueEntries[13] += (d_data[13]*coeffs[1] + d_data[34]*coeffs[2] + d_data[55]*coeffs[3]); // m=-1 + m=0 + m=1 
        uniqueEntries[14] += (d_data[14]*coeffs[1] + d_data[35]*coeffs[2] + d_data[56]*coeffs[3]); // m=-1 + m=0 + m=1 
        uniqueEntries[15] += (d_data[15]*coeffs[1] + d_data[36]*coeffs[2] + d_data[57]*coeffs[3]); // m=-1 + m=0 + m=1 
        uniqueEntries[16] += (d_data[16]*coeffs[1] + d_data[37]*coeffs[2] + d_data[58]*coeffs[3]); // m=-1 + m=0 + m=1 
        uniqueEntries[17] += (d_data[17]*coeffs[1] + d_data[38]*coeffs[2] + d_data[59]*coeffs[3]); // m=-1 + m=0 + m=1 
        uniqueEntries[18] += (d_data[18]*coeffs[1] + d_data[39]*coeffs[2] + d_data[60]*coeffs[3]); // m=-1 + m=0 + m=1 
        uniqueEntries[19] += (d_data[19]*coeffs[1] + d_data[40]*coeffs[2] + d_data[61]*coeffs[3]); // m=-1 + m=0 + m=1 
        uniqueEntries[20] += (d_data[20]*coeffs[1] + d_data[41]*coeffs[2] + d_data[62]*coeffs[3]); // m=-1 + m=0 + m=1 

        uniqueEntries[0] += q_data[0]*coeffs[4] + q_data[21]*coeffs[5] + q_data[42]*coeffs[6] + q_data[63]*coeffs[7] + q_data[84]*coeffs[8]; // sum(m=-l, m=l)
        uniqueEntries[1] += q_data[1]*coeffs[4] + q_data[22]*coeffs[5] + q_data[43]*coeffs[6] + q_data[64]*coeffs[7] + q_data[85]*coeffs[8]; // sum(m=-l, m=l)
        uniqueEntries[2] += q_data[2]*coeffs[4] + q_data[23]*coeffs[5] + q_data[44]*coeffs[6] + q_data[65]*coeffs[7] + q_data[86]*coeffs[8]; // sum(m=-l, m=l)
        uniqueEntries[3] += q_data[3]*coeffs[4] + q_data[24]*coeffs[5] + q_data[45]*coeffs[6] + q_data[66]*coeffs[7] + q_data[87]*coeffs[8]; // sum(m=-l, m=l)
        uniqueEntries[4] += q_data[4]*coeffs[4] + q_data[25]*coeffs[5] + q_data[46]*coeffs[6] + q_data[67]*coeffs[7] + q_data[88]*coeffs[8]; // sum(m=-l, m=l)
        uniqueEntries[5] += q_data[5]*coeffs[4] + q_data[26]*coeffs[5] + q_data[47]*coeffs[6] + q_data[68]*coeffs[7] + q_data[89]*coeffs[8]; // sum(m=-l, m=l)
        uniqueEntries[6] += q_data[6]*coeffs[4] + q_data[27]*coeffs[5] + q_data[48]*coeffs[6] + q_data[69]*coeffs[7] + q_data[90]*coeffs[8]; // sum(m=-l, m=l)
        uniqueEntries[7] += q_data[7]*coeffs[4] + q_data[28]*coeffs[5] + q_data[49]*coeffs[6] + q_data[70]*coeffs[7] + q_data[91]*coeffs[8]; // sum(m=-l, m=l)
        uniqueEntries[8] += q_data[8]*coeffs[4] + q_data[29]*coeffs[5] + q_data[50]*coeffs[6] + q_data[71]*coeffs[7] + q_data[92]*coeffs[8]; // sum(m=-l, m=l)
        uniqueEntries[9] += q_data[9]*coeffs[4] + q_data[30]*coeffs[5] + q_data[51]*coeffs[6] + q_data[72]*coeffs[7] + q_data[93]*coeffs[8]; // sum(m=-l, m=l)
        uniqueEntries[10] += q_data[10]*coeffs[4] + q_data[31]*coeffs[5] + q_data[52]*coeffs[6] + q_data[73]*coeffs[7] + q_data[94]*coeffs[8]; // sum(m=-l, m=l)
        uniqueEntries[11] += q_data[11]*coeffs[4] + q_data[32]*coeffs[5] + q_data[53]*coeffs[6] + q_data[74]*coeffs[7] + q_data[95]*coeffs[8]; // sum(m=-l, m=l)
        uniqueEntries[12] += q_data[12]*coeffs[4] + q_data[33]*coeffs[5] + q_data[54]*coeffs[6] + q_data[75]*coeffs[7] + q_data[96]*coeffs[8]; // sum(m=-l, m=l)
        uniqueEntries[13] += q_data[13]*coeffs[4] + q_data[34]*coeffs[5] + q_data[55]*coeffs[6] + q_data[76]*coeffs[7] + q_data[97]*coeffs[8]; // sum(m=-l, m=l)
        uniqueEntries[14] += q_data[14]*coeffs[4] + q_data[35]*coeffs[5] + q_data[56]*coeffs[6] + q_data[77]*coeffs[7] + q_data[98]*coeffs[8]; // sum(m=-l, m=l)
        uniqueEntries[15] += q_data[15]*coeffs[4] + q_data[36]*coeffs[5] + q_data[57]*coeffs[6] + q_data[78]*coeffs[7] + q_data[99]*coeffs[8]; // sum(m=-l, m=l)
        uniqueEntries[16] += q_data[16]*coeffs[4] + q_data[37]*coeffs[5] + q_data[58]*coeffs[6] + q_data[79]*coeffs[7] + q_data[100]*coeffs[8]; // sum(m=-l, m=l)
        uniqueEntries[17] += q_data[17]*coeffs[4] + q_data[38]*coeffs[5] + q_data[59]*coeffs[6] + q_data[80]*coeffs[7] + q_data[101]*coeffs[8]; // sum(m=-l, m=l)
        uniqueEntries[18] += q_data[18]*coeffs[4] + q_data[39]*coeffs[5] + q_data[60]*coeffs[6] + q_data[81]*coeffs[7] + q_data[102]*coeffs[8]; // sum(m=-l, m=l)
        uniqueEntries[19] += q_data[19]*coeffs[4] + q_data[40]*coeffs[5] + q_data[61]*coeffs[6] + q_data[82]*coeffs[7] + q_data[103]*coeffs[8]; // sum(m=-l, m=l)
        uniqueEntries[20] += q_data[20]*coeffs[4] + q_data[41]*coeffs[5] + q_data[62]*coeffs[6] + q_data[83]*coeffs[7] + q_data[104]*coeffs[8]; // sum(m=-l, m=l)

        uniqueEntries[0] += o_data[0]*coeffs[9] + o_data[21]*coeffs[10] + o_data[42]*coeffs[11] + o_data[63]*coeffs[12] + o_data[84]*coeffs[13] + o_data[105]*coeffs[14] + o_data[126]*coeffs[15]; // sum(m=-l, m=l) 
        uniqueEntries[1] += o_data[1]*coeffs[9] + o_data[22]*coeffs[10] + o_data[43]*coeffs[11] + o_data[64]*coeffs[12] + o_data[85]*coeffs[13] + o_data[106]*coeffs[14] + o_data[127]*coeffs[15]; // sum(m=-l, m=l) 
        uniqueEntries[2] += o_data[2]*coeffs[9] + o_data[23]*coeffs[10] + o_data[44]*coeffs[11] + o_data[65]*coeffs[12] + o_data[86]*coeffs[13] + o_data[107]*coeffs[14] + o_data[128]*coeffs[15]; // sum(m=-l, m=l) 
        uniqueEntries[3] += o_data[3]*coeffs[9] + o_data[24]*coeffs[10] + o_data[45]*coeffs[11] + o_data[66]*coeffs[12] + o_data[87]*coeffs[13] + o_data[108]*coeffs[14] + o_data[129]*coeffs[15]; // sum(m=-l, m=l) 
        uniqueEntries[4] += o_data[4]*coeffs[9] + o_data[25]*coeffs[10] + o_data[46]*coeffs[11] + o_data[67]*coeffs[12] + o_data[88]*coeffs[13] + o_data[109]*coeffs[14] + o_data[130]*coeffs[15]; // sum(m=-l, m=l) 
        uniqueEntries[5] += o_data[5]*coeffs[9] + o_data[26]*coeffs[10] + o_data[47]*coeffs[11] + o_data[68]*coeffs[12] + o_data[89]*coeffs[13] + o_data[110]*coeffs[14] + o_data[131]*coeffs[15]; // sum(m=-l, m=l) 
        uniqueEntries[6] += o_data[6]*coeffs[9] + o_data[27]*coeffs[10] + o_data[48]*coeffs[11] + o_data[69]*coeffs[12] + o_data[90]*coeffs[13] + o_data[111]*coeffs[14] + o_data[132]*coeffs[15]; // sum(m=-l, m=l) 
        uniqueEntries[7] += o_data[7]*coeffs[9] + o_data[28]*coeffs[10] + o_data[49]*coeffs[11] + o_data[70]*coeffs[12] + o_data[91]*coeffs[13] + o_data[112]*coeffs[14] + o_data[133]*coeffs[15]; // sum(m=-l, m=l) 
        uniqueEntries[8] += o_data[8]*coeffs[9] + o_data[29]*coeffs[10] + o_data[50]*coeffs[11] + o_data[71]*coeffs[12] + o_data[92]*coeffs[13] + o_data[113]*coeffs[14] + o_data[134]*coeffs[15]; // sum(m=-l, m=l) 
        uniqueEntries[9] += o_data[9]*coeffs[9] + o_data[30]*coeffs[10] + o_data[51]*coeffs[11] + o_data[72]*coeffs[12] + o_data[93]*coeffs[13] + o_data[114]*coeffs[14] + o_data[135]*coeffs[15]; // sum(m=-l, m=l) 
        uniqueEntries[10] += o_data[10]*coeffs[9] + o_data[31]*coeffs[10] + o_data[52]*coeffs[11] + o_data[73]*coeffs[12] + o_data[94]*coeffs[13] + o_data[115]*coeffs[14] + o_data[136]*coeffs[15]; // sum(m=-l, m=l) 
        uniqueEntries[11] += o_data[11]*coeffs[9] + o_data[32]*coeffs[10] + o_data[53]*coeffs[11] + o_data[74]*coeffs[12] + o_data[95]*coeffs[13] + o_data[116]*coeffs[14] + o_data[137]*coeffs[15]; // sum(m=-l, m=l) 
        uniqueEntries[12] += o_data[12]*coeffs[9] + o_data[33]*coeffs[10] + o_data[54]*coeffs[11] + o_data[75]*coeffs[12] + o_data[96]*coeffs[13] + o_data[117]*coeffs[14] + o_data[138]*coeffs[15]; // sum(m=-l, m=l) 
        uniqueEntries[13] += o_data[13]*coeffs[9] + o_data[34]*coeffs[10] + o_data[55]*coeffs[11] + o_data[76]*coeffs[12] + o_data[97]*coeffs[13] + o_data[118]*coeffs[14] + o_data[139]*coeffs[15]; // sum(m=-l, m=l) 
        uniqueEntries[14] += o_data[14]*coeffs[9] + o_data[35]*coeffs[10] + o_data[56]*coeffs[11] + o_data[77]*coeffs[12] + o_data[98]*coeffs[13] + o_data[119]*coeffs[14] + o_data[140]*coeffs[15]; // sum(m=-l, m=l) 
        uniqueEntries[15] += o_data[15]*coeffs[9] + o_data[36]*coeffs[10] + o_data[57]*coeffs[11] + o_data[78]*coeffs[12] + o_data[99]*coeffs[13] + o_data[120]*coeffs[14] + o_data[141]*coeffs[15]; // sum(m=-l, m=l) 
        uniqueEntries[16] += o_data[16]*coeffs[9] + o_data[37]*coeffs[10] + o_data[58]*coeffs[11] + o_data[79]*coeffs[12] + o_data[100]*coeffs[13] + o_data[121]*coeffs[14] + o_data[142]*coeffs[15]; // sum(m=-l, m=l) 
        uniqueEntries[17] += o_data[17]*coeffs[9] + o_data[38]*coeffs[10] + o_data[59]*coeffs[11] + o_data[80]*coeffs[12] + o_data[101]*coeffs[13] + o_data[122]*coeffs[14] + o_data[143]*coeffs[15]; // sum(m=-l, m=l) 
        uniqueEntries[18] += o_data[18]*coeffs[9] + o_data[39]*coeffs[10] + o_data[60]*coeffs[11] + o_data[81]*coeffs[12] + o_data[102]*coeffs[13] + o_data[123]*coeffs[14] + o_data[144]*coeffs[15]; // sum(m=-l, m=l) 
        uniqueEntries[19] += o_data[19]*coeffs[9] + o_data[40]*coeffs[10] + o_data[61]*coeffs[11] + o_data[82]*coeffs[12] + o_data[103]*coeffs[13] + o_data[124]*coeffs[14] + o_data[145]*coeffs[15]; // sum(m=-l, m=l) 
        uniqueEntries[20] += o_data[20]*coeffs[9] + o_data[41]*coeffs[10] + o_data[62]*coeffs[11] + o_data[83]*coeffs[12] + o_data[104]*coeffs[13] + o_data[125]*coeffs[14] + o_data[146]*coeffs[15]; // sum(m=-l, m=l) 

        uniqueEntries[0] += h_data[0]*coeffs[16] + h_data[21]*coeffs[17] + h_data[42]*coeffs[18] + h_data[63]*coeffs[19] + h_data[84]*coeffs[20] + h_data[105]*coeffs[21] + h_data[126]*coeffs[22] + h_data[147]*coeffs[23] + h_data[168]*coeffs[24]; // sum(m=-l, m=l)
        uniqueEntries[1] += h_data[1]*coeffs[16] + h_data[22]*coeffs[17] + h_data[43]*coeffs[18] + h_data[64]*coeffs[19] + h_data[85]*coeffs[20] + h_data[106]*coeffs[21] + h_data[127]*coeffs[22] + h_data[148]*coeffs[23] + h_data[169]*coeffs[24]; // sum(m=-l, m=l)
        uniqueEntries[2] += h_data[2]*coeffs[16] + h_data[23]*coeffs[17] + h_data[44]*coeffs[18] + h_data[65]*coeffs[19] + h_data[86]*coeffs[20] + h_data[107]*coeffs[21] + h_data[128]*coeffs[22] + h_data[149]*coeffs[23] + h_data[170]*coeffs[24]; // sum(m=-l, m=l)
        uniqueEntries[3] += h_data[3]*coeffs[16] + h_data[24]*coeffs[17] + h_data[45]*coeffs[18] + h_data[66]*coeffs[19] + h_data[87]*coeffs[20] + h_data[108]*coeffs[21] + h_data[129]*coeffs[22] + h_data[150]*coeffs[23] + h_data[171]*coeffs[24]; // sum(m=-l, m=l)
        uniqueEntries[4] += h_data[4]*coeffs[16] + h_data[25]*coeffs[17] + h_data[46]*coeffs[18] + h_data[67]*coeffs[19] + h_data[88]*coeffs[20] + h_data[109]*coeffs[21] + h_data[130]*coeffs[22] + h_data[151]*coeffs[23] + h_data[172]*coeffs[24]; // sum(m=-l, m=l)
        uniqueEntries[5] += h_data[5]*coeffs[16] + h_data[26]*coeffs[17] + h_data[47]*coeffs[18] + h_data[68]*coeffs[19] + h_data[89]*coeffs[20] + h_data[110]*coeffs[21] + h_data[131]*coeffs[22] + h_data[152]*coeffs[23] + h_data[173]*coeffs[24]; // sum(m=-l, m=l)
        uniqueEntries[6] += h_data[6]*coeffs[16] + h_data[27]*coeffs[17] + h_data[48]*coeffs[18] + h_data[69]*coeffs[19] + h_data[90]*coeffs[20] + h_data[111]*coeffs[21] + h_data[132]*coeffs[22] + h_data[153]*coeffs[23] + h_data[174]*coeffs[24]; // sum(m=-l, m=l)
        uniqueEntries[7] += h_data[7]*coeffs[16] + h_data[28]*coeffs[17] + h_data[49]*coeffs[18] + h_data[70]*coeffs[19] + h_data[91]*coeffs[20] + h_data[112]*coeffs[21] + h_data[133]*coeffs[22] + h_data[154]*coeffs[23] + h_data[175]*coeffs[24]; // sum(m=-l, m=l)
        uniqueEntries[8] += h_data[8]*coeffs[16] + h_data[29]*coeffs[17] + h_data[50]*coeffs[18] + h_data[71]*coeffs[19] + h_data[92]*coeffs[20] + h_data[113]*coeffs[21] + h_data[134]*coeffs[22] + h_data[155]*coeffs[23] + h_data[176]*coeffs[24]; // sum(m=-l, m=l)
        uniqueEntries[9] += h_data[9]*coeffs[16] + h_data[30]*coeffs[17] + h_data[51]*coeffs[18] + h_data[72]*coeffs[19] + h_data[93]*coeffs[20] + h_data[114]*coeffs[21] + h_data[135]*coeffs[22] + h_data[156]*coeffs[23] + h_data[177]*coeffs[24]; // sum(m=-l, m=l)
        uniqueEntries[10] += h_data[10]*coeffs[16] + h_data[31]*coeffs[17] + h_data[52]*coeffs[18] + h_data[73]*coeffs[19] + h_data[94]*coeffs[20] + h_data[115]*coeffs[21] + h_data[136]*coeffs[22] + h_data[157]*coeffs[23] + h_data[178]*coeffs[24]; // sum(m=-l, m=l)
        uniqueEntries[11] += h_data[11]*coeffs[16] + h_data[32]*coeffs[17] + h_data[53]*coeffs[18] + h_data[74]*coeffs[19] + h_data[95]*coeffs[20] + h_data[116]*coeffs[21] + h_data[137]*coeffs[22] + h_data[158]*coeffs[23] + h_data[179]*coeffs[24]; // sum(m=-l, m=l)
        uniqueEntries[12] += h_data[12]*coeffs[16] + h_data[33]*coeffs[17] + h_data[54]*coeffs[18] + h_data[75]*coeffs[19] + h_data[96]*coeffs[20] + h_data[117]*coeffs[21] + h_data[138]*coeffs[22] + h_data[159]*coeffs[23] + h_data[180]*coeffs[24]; // sum(m=-l, m=l)
        uniqueEntries[13] += h_data[13]*coeffs[16] + h_data[34]*coeffs[17] + h_data[55]*coeffs[18] + h_data[76]*coeffs[19] + h_data[97]*coeffs[20] + h_data[118]*coeffs[21] + h_data[139]*coeffs[22] + h_data[160]*coeffs[23] + h_data[181]*coeffs[24]; // sum(m=-l, m=l)
        uniqueEntries[14] += h_data[14]*coeffs[16] + h_data[35]*coeffs[17] + h_data[56]*coeffs[18] + h_data[77]*coeffs[19] + h_data[98]*coeffs[20] + h_data[119]*coeffs[21] + h_data[140]*coeffs[22] + h_data[161]*coeffs[23] + h_data[182]*coeffs[24]; // sum(m=-l, m=l)
        uniqueEntries[15] += h_data[15]*coeffs[16] + h_data[36]*coeffs[17] + h_data[57]*coeffs[18] + h_data[78]*coeffs[19] + h_data[99]*coeffs[20] + h_data[120]*coeffs[21] + h_data[141]*coeffs[22] + h_data[162]*coeffs[23] + h_data[183]*coeffs[24]; // sum(m=-l, m=l)
        uniqueEntries[16] += h_data[16]*coeffs[16] + h_data[37]*coeffs[17] + h_data[58]*coeffs[18] + h_data[79]*coeffs[19] + h_data[100]*coeffs[20] + h_data[121]*coeffs[21] + h_data[142]*coeffs[22] + h_data[163]*coeffs[23] + h_data[184]*coeffs[24]; // sum(m=-l, m=l)
        uniqueEntries[17] += h_data[17]*coeffs[16] + h_data[38]*coeffs[17] + h_data[59]*coeffs[18] + h_data[80]*coeffs[19] + h_data[101]*coeffs[20] + h_data[122]*coeffs[21] + h_data[143]*coeffs[22] + h_data[164]*coeffs[23] + h_data[185]*coeffs[24]; // sum(m=-l, m=l)
        uniqueEntries[18] += h_data[18]*coeffs[16] + h_data[39]*coeffs[17] + h_data[60]*coeffs[18] + h_data[81]*coeffs[19] + h_data[102]*coeffs[20] + h_data[123]*coeffs[21] + h_data[144]*coeffs[22] + h_data[165]*coeffs[23] + h_data[186]*coeffs[24]; // sum(m=-l, m=l)
        uniqueEntries[19] += h_data[19]*coeffs[16] + h_data[40]*coeffs[17] + h_data[61]*coeffs[18] + h_data[82]*coeffs[19] + h_data[103]*coeffs[20] + h_data[124]*coeffs[21] + h_data[145]*coeffs[22] + h_data[166]*coeffs[23] + h_data[187]*coeffs[24]; // sum(m=-l, m=l)
        uniqueEntries[20] += h_data[20]*coeffs[16] + h_data[41]*coeffs[17] + h_data[62]*coeffs[18] + h_data[83]*coeffs[19] + h_data[104]*coeffs[20] + h_data[125]*coeffs[21] + h_data[146]*coeffs[22] + h_data[167]*coeffs[23] + h_data[188]*coeffs[24]; // sum(m=-l, m=l)
        generate_full_matrix(21,uniqueEntries, result_vec);       
    }
    Py_RETURN_NONE;
} 

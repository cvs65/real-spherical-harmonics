import diff_rsh
import numpy as np

coeffs = np.array([0.28209479177387814,
                    0.48860251190291992,
                    0.48860251190291992,
                    0.48860251190291992,
                    0.7283656203947194,
                    0.7283656203947194,
                    0.6307831305050401,
                    0.7283656203947194,
                    0.3641828101973597,
                    0.23601743597065739,
                    1.1562445770562215,
                    0.9140915989289315,
                    0.7463526651802308,
                    0.9140915989289315,
                    0.5781222885281108,
                    0.23601743597065739,                                                        
                    0.5721926724106754,
                    0.4046013188068413,
                    1.5138795132120961,
                    1.0704744696916628,
                    0.8462843753216345,
                    1.0704744696916628,
                    0.7569397566060481,
                    0.4046013188068413,
                    0.14304816810266885]
                    )
cm = np.array([np.array([10.0,7.5,20.0])])
def get_analytical_RSH(a, cm, center, Rc, fast):
    RSH_d1 = np.ascontiguousarray(np.zeros((cm.ndim-1, 3)))
    RSH_d2 = np.ascontiguousarray(np.zeros((cm.ndim-1, 3, 3)))
    RSH_d3 = np.ascontiguousarray(np.zeros((cm.ndim-1, 3, 3, 3)))
    RSH_d4 = np.ascontiguousarray(np.zeros((cm.ndim-1, 3, 3, 3, 3)))
    RSH_d5 = np.ascontiguousarray(np.zeros((cm.ndim-1, 3, 3, 3, 3, 3)))
    cells = np.array([2, 2, 0])
    if fast:
       diff_rsh.diff_1_and_2(a, cm, RSH_d1, RSH_d2, np.array(coeffs), center, cells, Rc)
       return [RSH_d1, RSH_d2]
    else:
        diff_rsh.diff_1_and_2(a, cm, RSH_d1, RSH_d2, np.array(coeffs), center, cells, Rc)
        diff_rsh.diff_3(a, cm, RSH_d3, np.array(coeffs), center, cells, Rc)
        diff_rsh.diff_4(a, cm, RSH_d4, np.array(coeffs), center, cells, Rc)
        diff_rsh.diff_5(a, cm, RSH_d5, np.array(coeffs), center, cells, Rc)
        return [RSH_d1, RSH_d2, RSH_d3, RSH_d4, RSH_d5]

RSH = get_analytical_RSH(a=59, cm=cm, center=np.array([9.0,8.0,13.0]), Rc=np.array([18.0,16.0,26.0]), fast=False)
print(RSH[1])

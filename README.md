# Real Spherical Harmonics

## Introduction
This repository aims to address the complex calculation of higher order derivatives of the gaussian spherical harmonics, used in diversed scientific fields.
The prerequisites of this package are listed below. The repository is provided without any warranty nor guarantee for operativeness / compatibility. If you find a bug / issue, feel free to report to the email given
below or open an issue.
Cheers,
Julian

Julian Beßner, M.Ed.
Institute of Electrochemistry
Ulm University
Albert-Einstein-Allee 47
89134 Ulm, Germany
julian.bessner [at] uni-ulm [dot] de

## Description
In order to calculate the potential $v(r)$ of a given density $n(r)$, we use the following Coulomb equation:
$$
v(r) = \int \frac{n(r')}{\lvert r - r' \rvert} \, dr'.
$$
Here, we express the density $n(r)$ by the generalized Gaussian containing the real solid harmonics. The Gaussian spherical harmonics are defined as:
$$
    g_L(r) = g_l(r) Y_L(\vec{r}), 
$$
where $g_l(r)$ are the Gaussian defined by:
$$
    g_l(r) = \frac{1}{\sqrt{4\pi}} \frac{l!}{(2l + 1)!}(4\alpha)^{l + \frac{3}{2}}r^l e^{-\alpha r^2} \tag{2}
$$
and the spherical harmonics (SH) are written as:
$$
    Y_l^m(\theta, \phi) = (-1)^{\frac{m - \lvert m \rvert}{2}} \sqrt{\frac{2l + 1}{4 \pi}\frac{(l - \lvert m \rvert)!}{(l + \lvert m \rvert)!}} P_l^{\lvert m \rvert}(\cos{\theta}) e^{im\phi}. \tag{3}
$$
$P_l^{\lvert m \rvert}(z)$ are the associated Legendre polynomials
$$
P_l^{m}(z) = \frac{(-1)^m}{2^ll!} (1 - z^2)^{\frac{m}{2}} \frac{d^{l + m}}{dz^{l + m}} (z^2 - 1)^l. \tag{4}
$$
$l$ is the orbital angular momentum quantum number and $m$ is the magnetic quantum number.
Now, we want to calculate the real spherical haromincs (RSH). To this end, we consider only the real part of the SH for the positive and the imaginary part for the negative m values.

$$
y^0_l = Y^0_l,
$$
$$
y^m_l =
\begin{cases}
(Y^m_l + Y^{m\ast}_l) / \sqrt{2} = \sqrt{2} \Re(Y^m_l) \quad &, m > 0, \\
(Y^{\lvert m \rvert}_l + Y^{\lvert m \rvert \ast}_l) / i \sqrt{2} = \sqrt{2} \Im(Y^{\lvert m \rvert}_l) \quad &, m < 0. \tag{5}
\end{cases}
$$
There is also an explicit formula to calculate the RSH:
$$
y^m_l(\theta, \phi) = C^{\lvert m \rvert}_l P^{\lvert m \rvert}(\cos(\theta)) \Phi_m(\phi), \tag{6}
$$
where 
$$
C^m_l = 
\begin{cases}
\sqrt{\frac{2l + 1}{4\Pi}} \quad &, m = 0, \\
\sqrt{\frac{2l + 1}{2\Pi} \frac{(l - m)!}{(l + m)!}} \quad &, m > 0 \tag{7}
\end{cases}
$$
and 
$$
\Phi_m(\phi) = 
\begin{cases}
\cos(m\phi) \quad &, m \geq 0, \\
\sin(\lvert m \rvert \phi) \quad &, m < 0. \tag{8}
\end{cases}
$$

Now, we only need to multiply the sum of the RSSH $y^m_l$ over $m$ by the Gaussians (2) to get the generalized Gaussians $g_L(r)$ for the potential $V_L(r)$.
The potential is given by
$$
V_L(r) = \int \frac{g_l(r')y_L(\theta, \phi)}{\lvert r - r' \rvert} \, dr'.
$$
Notice that the potential is a product of the radial part $g_l(r')$ and the angular part $y_L(\theta, \phi)$. The angular part $y_L$ is the sum defined as follows,
$$
y_L(\theta, \phi) = \sum_{m = -l}^{l} y^m_l (\theta, \phi).
$$
The angular part is not depending on $r$, therefore we only integrate the radial part which is not depending on $m$ and thus we only need to evaluate the integral once for each quantum number $l$. The radial potential $v_l(r)$ is calculated by,
$$
v_l(r) = \frac{4 \pi}{2l + 1} \left[ \int_0^r g_l(r')r'(\frac{r'}{r})^{l+1} \, dr' + \int_r^{\infty} g_l(r')r'(\frac{r}{r'})^{l} \, dr' \right].
$$

Finally, we can combine the radial part and the angular part of the potential together to calculate the potential $V_L(r)$.
Now, we have calculated the potential using the generalized Gaussian functions for the dipole $(l = 1)$, the quadrupole $(l = 2)$, the octupole $(l = 3)$, and the hexadecapole $(l = 4)$, according to [Rostgaard, MSc. Thesis, 2006](https://wiki.fysik.dtu.dk/gpaw/_downloads/b769d4e6a7c3951e5917cda63e27695e/rostgaard_master.pdf) (Appendix D.2, p. 88f). As, the multipole components of the potential decrease with $1/r^{l+1}$, we neglected the 32-pole. Eventually, we want to build the gradient of the function for each $l$ value. 
As a result, we get a vector, containing the partial derivative of x, y and z respectively, for each $l$ value. 
For the dipole we have three magnetic quantum numbers, thus we get a three dimensional vector for $v_1(x,y,z)$:
$$
v_1(x,y,z) = \left(\begin{array}{c} v_1^{-1}(x,y,z) \\ v_1^0(x,y,z) \\ v_1^1(x,y,z) \end{array}\right)
$$
Furthermore, each potential $v_1^m(x,y,z)$ can be differentiated, which leads to a three dimensional vector for each $v_1^m(x,y,z)$.
$$
\nabla v_1^m(x,y,z) = \left(\begin{array}{c} \partial_x v_1^m(x,y,z) \\ \partial_y v_1^m(x,y,z) \\ \partial_z v_1^m(x,y,z) \end{array}\right)
$$
At last, we need the sum over the differentiated potentials $v_1^i(x,y,z)$, where each summand is multiplied by a factor $D_m$ derived by the moments of the density.
$$
\nabla v_1(x,y,z) = \sum_{m=-1}^1 D_m*\left(\begin{array}{c} \partial_x v_1^m(x,y,z) \\ \partial_y v_1^m(x,y,z) \\ \partial_z v_1^m(x,y,z) \end{array}\right) = \left(\begin{array}{c} \partial_x v_1^{-1}(x,y,z)*D_{-1} + \partial_x v_1^0(x,y,z)*D_0 + \partial_x v_1^1(x,y,z)*D_1\\ \partial_y v_1^{-1}(x,y,z)*D_{-1} + \partial_y v_1^0(x,y,z)*D_0 + \partial_y v_1^1(x,y,z)*D_1 \\ \partial_z v_1^{-1}(x,y,z)*D_{-1} + \partial_z v_1^0(x,y,z)*D_0 + \partial_z v_1^1(x,y,z)*D_1 \end{array}\right)
$$
In the code, the derivatives are calculated with respect to a center and an observer point. The distance between the two points $r_o$ is used to evaluate the derivatives. Furthermore, there can be multiple centers, so that the derivatives are calculated for each center and then summed up. Beyond that, the distance $r_o$ can be expanded by integer multiples of the cell vector $R_c$. In the case of $R_c = 0$, the distance vector $r_o$ is not expanded. The image shows a schematic visualization of the distance vector and the effect of the expansion.
![Near Neighbor Cells Visualization](Near_Neighbor_Cell_Visualization.png)
The derivatives of the RSH are stored in separate files. The fifth derivative is split into several helper functions to reduce the files' sizes and optimize the code's performance. Additionally, the helper functions are stored in the file "utilities.c". The c-files for the calculation of the derivatives only calculate the independent values and the helper functions in the "utilities.c" file are then used to fill the symmetric matrices.

## Installation
As a prerequisite, one need to have a C compiler installed on the machine. This can be achieved by using a toolchain like [EasyBuild](https://docs.easybuild.io/). Now, one just need to follow the steps:
1) Clone the repository to your local machine:
```bash
git clone https://gitlab.com/cvs65/Higher-Order-Derivatives-of-Real-Spherical-Harmonics.git
```
2) Before compiling the code, one have to adjust the headers in the files in the c-folder. Then the code can be compiled, using the following command:
```bash
python3 setup.py build
```
3) Install the package, using the following command:
```bash
python3 setup.py install --user
```
4) Import the C module in the python file to use the RSH:
```bash
import diff_rsh 
```

## Usage
Here is an example script on how to use the RSH in Python.
```python
import diff_rsh
import numpy as np

## Coefficients used to transform the RSH to cartesian space
coeffs = np.array([0.28209479177387814,
                    0.48860251190291992,
                    0.48860251190291992,
                    0.48860251190291992,
                    0.7283656203947194,
                    0.7283656203947194,
                    0.6307831305050401,
                    0.7283656203947194,
                    0.3641828101973597,
                    0.23601743597065739,
                    1.1562445770562215,
                    0.9140915989289315,
                    0.7463526651802308,
                    0.9140915989289315,
                    0.5781222885281108,
                    0.23601743597065739,                                                        
                    0.5721926724106754,
                    0.4046013188068413,
                    1.5138795132120961,
                    1.0704744696916628,
                    0.8462843753216345,
                    1.0704744696916628,
                    0.7569397566060481,
                    0.4046013188068413,
                    0.14304816810266885]
                    )

## Define the observer point for the RSH, this is equal to the r' in the equations in the description part.
obs = np.array([np.array([10,7.5,20])])

## An example of how the RSH can be implemented as a function in a code
def get_analytical_RSH(a, obs, center, Rc, fast):
    # Initialize arrays filled with zeros, according to the size of the derivative
    RSH_d1 = np.ascontiguousarray(np.zeros((1, 3)))
    RSH_d2 = np.ascontiguousarray(np.zeros((1, 3, 3)))
    RSH_d3 = np.ascontiguousarray(np.zeros((1, 3, 3, 3)))
    RSH_d4 = np.ascontiguousarray(np.zeros((1, 3, 3, 3, 3)))
    RSH_d5 = np.ascontiguousarray(np.zeros((1, 3, 3, 3, 3, 3)))
    # This is the image in which the observer point is located.
    # 
    cells = np.array([2, 2, 0])
    if fast:
       diff_rsh.diff_1_and_2(a, obs, RSH_d1, RSH_d2, np.array(coeffs), center, cells, Rc)
       return [RSH_d1, RSH_d2]
    else:
        diff_rsh.diff_1_and_2(a, obs, RSH_d1, RSH_d2, np.array(coeffs), center, cells, Rc)
        diff_rsh.diff_3(a, obs, RSH_d3, np.array(coeffs), center, cells, Rc)
        diff_rsh.diff_4(a, obs, RSH_d4, np.array(coeffs), center, cells, Rc)
        diff_rsh.diff_5(a, obs, RSH_d5, np.array(coeffs), center, cells, Rc)
        return [RSH_d1, RSH_d2, RSH_d3, RSH_d4, RSH_d5]


## Call the function and specify the input parameters
# a is the width of the gaussians
# center is the position of the gaussian
# Rc is the cell parameter 
# fast let you choose between calculating the only the first and the second derivative, or all derivatives up to the fifth.
RSH = get_analytical_RSH(a=59, cm=cm, center=np.array([9.26287811,8.0218874,13.00965471]), Rc=np.array([18.52575622,16.04377481,26.01930942]), fast=False)
## Print the Gradient of the RSH
print(RSH[0]) 
```


## Support
In case you need help to get this package to run or implemented in another software, feel free to contact me (julian.bessner@uni-ulm.de).

## Roadmap
So far this package is implemented in the open source software [GPAW](https://wiki.fysik.dtu.dk/gpaw/index.html) to optimize the performance of the code. Currently, it is used to replace the numerical calculation of the multipoles on the grid. The analytical solution of the gaussian harmonics enables a faster performance of the grid-based calulation.

## Authors and acknowledgment
This code is a result of the collaboration between the [group of Prof. Hannes Jónsson](https://hj.hi.is/researchgroup.html) of the University of Iceland and the [group of Prof. Timo Jacob](https://www.uni-ulm.de/nawi/nawi-elektrochemie/institut/mitarbeiter-des-instituts/) of the University Ulm. Especially, I'd like to thank Anoop A. Nair, Elvar Örn Jónsson from the University of Iceland and Björn Kirchhoff from the Universtiy Ulm, for their great contribution to this develop this code.

## License
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a> <br />
This code is licensed under [Creative Commons Attribution-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-sa/4.0/).

## Project status
So far this repository contains the derivatives of the dipole, quadrupole, octupole, and hexadecapole up to the fifth derivative. The RSH can be extended by the 32-pole, but this takes quite a lot MBs.

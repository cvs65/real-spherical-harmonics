from distutils.core import setup, Extension
import os
from pathlib import Path

home = os.getcwd()

sources = Path('c').glob('*.c')
sources = [str(source) for source in sources]
sources.sort()
#print(sources)
setup(name = 'diff_rsh', version = '1.0', 
        ext_modules = [Extension('diff_rsh', sources)])